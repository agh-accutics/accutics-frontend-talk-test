export const move = (array: any[], to: number, from: number): any[] => {
  const clone = [...array];
  clone.splice(to, 0, clone.splice(from, 1)[0]);
  return clone;
};
