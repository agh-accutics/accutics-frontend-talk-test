import { v4 as uuid } from "uuid";
import { Option } from "../components/SelectField";
import { IPreviewValue } from "../contexts/PreviewContext";
import { FieldDomain, OptionDomain, RuleDomain } from "../lib/instances/field";

export const generateFieldId = (): string => uuid();

export const generateOptionId = (): string => uuid();

export const generateRuleId = (): string => uuid();

export const rulesToOptions = (rules: RuleDomain[]): Option[] => {
  return rules.map((rule) => ({
    label: rule.rule_value,
    value: rule.rule_id,
    children: rulesToOptions(rule.children),
  }));
};

export const fieldsToOptions = (fields: FieldDomain[]): Option[] => {
  return fields.map((field) => ({
    label: field.field_name,
    value: field.field_id,
  }));
};

export const fieldOptionsToOptions = (options: OptionDomain[]): Option[] => {
  return options.map((option) => ({
    label: option.option_label,
    value: option.option_value,
  }));
};

export const getLevelDashes = (level = 0): string =>
  new Array(level).fill("-").join("");

export const validateRule = (
  rules: RuleDomain[],
  data: Record<string, IPreviewValue>
): boolean => {
  // @TODO Needs to listen to the rules and check it up against the data from the form.
  return true;
};
