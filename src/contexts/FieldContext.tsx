import { createContext, useContext } from "react";

import { FieldDomain } from "../lib/instances/field";

const FieldContext = createContext<Pick<
  FieldDomain,
  "field_id" | "field_name"
> | null>(null);

export const useFieldContext = () => {
  return useContext(FieldContext);
};

export default FieldContext.Provider;
