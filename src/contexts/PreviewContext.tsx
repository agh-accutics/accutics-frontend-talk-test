import React, { createContext, useContext, useState, useCallback } from "react";

export type IPreviewValue = string | null;

type IPreviewContext = {
  data: Record<string, IPreviewValue>;
  set(data: Record<string, IPreviewValue>): void;
  get(key: string): IPreviewValue;
  onChange(key: string, value: IPreviewValue): void;
};

const PreviewContext = createContext<IPreviewContext | null>(null);

export const usePreviewContext = (): IPreviewContext => {
  return useContext(PreviewContext) as IPreviewContext;
};

type IPreviewContextProvider = {
  children: JSX.Element[];
};

const PreviewContextProvider: React.FC<IPreviewContextProvider> = ({
  children,
}) => {
  const [inputs, setInputs] = useState<Record<string, IPreviewValue>>({});

  const onChange = useCallback(
    (key: string, value: IPreviewValue) => {
      return setInputs((prev) => ({ ...prev, [key]: value }));
    },
    [setInputs]
  );

  const get = useCallback(
    (key: string): IPreviewValue => {
      return inputs[key];
    },
    [inputs]
  );

  return (
    <PreviewContext.Provider
      value={{
        data: inputs,
        set: setInputs,
        get,
        onChange,
      }}
    >
      <div className="preview-context-wrapper">{children}</div>
    </PreviewContext.Provider>
  );
};

export default React.memo(PreviewContextProvider);
