import React from "react";

import Fields from "../components/Fields";
import CreateFieldForm from "../components/CreateFieldForm";
import Container from "../components/Container";

function Home() {
  return (
    <Container>
      <div className="flex items-stretch flex-col-reverse md:flex-row">
        <div className="flex-grow flex-shrink">
          <Fields />
        </div>
        <div className="flex-shrink-0 w-1/5 ml-5">
          <CreateFieldForm />
        </div>
      </div>
    </Container>
  );
}

export default React.memo(Home);
