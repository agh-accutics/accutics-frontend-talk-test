import React from "react";

import Container from "../components/Container";
import FieldsTestPreviewForm from "../components/FieldsTestPreviewForm";

function Test() {
  return (
    <Container>
      <FieldsTestPreviewForm />
    </Container>
  );
}

export default React.memo(Test);
