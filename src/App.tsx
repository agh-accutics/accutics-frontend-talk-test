import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter } from "react-router-dom";

import Header from "./components/Header";
import Router from "./router";
import actions from "./store/actions";
import transformer from "./transformer";
import api from "./api";

function App() {
  const reduxDispatch = useDispatch();

  useEffect(() => {
    api.field.load().then((fields) => {
      reduxDispatch(
        actions.fields.setFields(transformer.fields.toDomains(fields))
      );
    });
    // eslint-disable-next-line
  }, []);
  return (
    <BrowserRouter>
      <div className="app">
        <Header className="app__header" />

        <Router />
      </div>
    </BrowserRouter>
  );
}

export default App;
