import React from "react";
import styled from "styled-components";

type Props = {
  children: string | JSX.Element;
};
/**
 * Name: Container
 */
const Container: React.FC<Props> = ({ children }) => {
  return <Wrapper className="container">{children}</Wrapper>;
};

export default React.memo(Container);

const Wrapper = styled.div`
  width: 100%;
  max-width: 1240px;
  margin: 0 auto;
  padding-left: 20px;
  padding-right: 20px;
`;
