import clsx from "clsx";
import React, { useCallback } from "react";
import styled from "styled-components";

type ButtonProps = {
  type?: "submit" | "button";
  className?: string;
  disabled?: boolean;
  color?: "primary" | "secondary" | "danger";
  size?: "default" | "small";
  children: string | JSX.Element;
  onClick?(event: React.MouseEvent<HTMLButtonElement>): void;
};

/**
 * Name: Button
 */
const Button: React.FC<ButtonProps> = React.forwardRef<
  HTMLButtonElement,
  ButtonProps
>(
  (
    {
      type,
      disabled,
      children,
      color = "primary",
      size = "default",
      className,
      onClick,
    },
    ref
  ) => {
    const prevent = useCallback(
      (event: React.MouseEvent<HTMLButtonElement>) => {
        if (!onClick) return;

        event.preventDefault();
        event.stopPropagation();

        return onClick(event);
      },
      [onClick]
    );

    return (
      <Wrapper
        ref={ref}
        className={clsx("button text-white", `button--${size}`, className, {
          "bg-blue-400": color === "primary",
          "bg-black": color === "secondary",
          "bg-red-400": color === "danger",

          "py-1.5 px-5 text-base": size === "default",
          "py-1 px-3 text-sm": size === "small",
        })}
        type={type}
        disabled={disabled}
        onClick={prevent}
      >
        {children}
      </Wrapper>
    );
  }
);

export default React.memo(Button);

const Wrapper = styled.button`
  &:focus,
  &:active {
    outline: none;
  }

  &:disabled {
    opacity: 0.5;
    cursor: inherit;
  }
`;
