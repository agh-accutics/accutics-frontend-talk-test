import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import Field from "./Field";
import selector from "../store/selector";
import actions from "../store/actions";
import Dragger from "./Dragger";

const Fields: React.FC = () => {
  const reduxDispatch = useDispatch();
  const fields = useSelector(selector.fields.all);

  const handleMove = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      reduxDispatch(actions.fields.moveField(dragIndex, hoverIndex));
    },
    [reduxDispatch]
  );

  return (
    <DndProvider backend={HTML5Backend}>
      <div className="fields">
        {fields.map((field, index) => (
          <Dragger
            type="field"
            key={field.field_id}
            className="field-dragger"
            id={field.field_id}
            index={index}
            meta={{
              field_id: field.field_id,
              field_name: field.field_name,
              index,
            }}
            Component={Field}
            onMove={handleMove}
          />
        ))}
      </div>
    </DndProvider>
  );
};

export default React.memo(Fields);
