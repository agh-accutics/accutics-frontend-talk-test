import clsx from "clsx";
import React from "react";
import styled from "styled-components";

export type Props = {
  label: string | JSX.Element;
  className?: string;
  required?: boolean;
};

/**
 * Name: FormLabel
 */
const FormLabel: React.FC<Props> = ({ className, label, required }) => {
  return (
    <Wrapper
      className={clsx(className, "text-base font-bold flex items-center")}
    >
      {label}
      {required && <span className="ml-auto">*</span>}
    </Wrapper>
  );
};

export default React.memo(FormLabel);

const Wrapper = styled.div``;
