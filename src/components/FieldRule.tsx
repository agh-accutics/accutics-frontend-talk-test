import React, { useCallback, useMemo } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import SelectField from "./SelectField";
import Button from "./Button";
import { RuleDomain } from "../lib/instances/field";
import { useFieldContext } from "../contexts/FieldContext";
import actions from "../store/actions";
import helpers from "../helpers";
import selector from "../store/selector";
import Dragger from "./Dragger";

type FieldRuleProps = {
  rule: RuleDomain;
  index: number;
  rule_position?: (number | string)[];
  onMove(
    from_index: number,
    to_index: number,
    position: (string | number)[]
  ): void;
  is_dragging: boolean;
  handle_ref: React.RefObject<HTMLDivElement>;
  handle_id: any;
};

/**
 * Name: FieldRule
 */
const FieldRule: React.FC<FieldRuleProps> = ({
  rule,
  rule_position = [],
  is_dragging,
  handle_ref,
  handle_id,
  onMove,
}) => {
  const reduxDispatch = useDispatch();
  const field = useFieldContext();
  const allFields = useSelector(selector.fields.all);

  const rule_position_children = useMemo(() => {
    return [...rule_position, "children"];
  }, [rule_position]);

  const fieldOptions = useMemo(() => {
    if (!allFields) return [];

    return helpers.fields.fieldsToOptions(
      allFields.filter((f) => f.field_id !== field?.field_id)
    );
  }, [allFields, field?.field_id]);

  const fieldOptionOptions = useMemo(() => {
    if (!allFields) return [];
    const selectedField = allFields.find(
      (f) => f.field_id === rule.rule_field_id
    );
    if (!selectedField) return [];

    return helpers.fields.fieldOptionsToOptions(selectedField.options);
  }, [rule.rule_field_id, allFields]);

  const handleChange = useCallback(
    (key: keyof RuleDomain, value: string) => {
      if (!field?.field_id) return;
      reduxDispatch(
        actions.fields.updateFieldRule(field.field_id, rule_position, {
          [key]: value,
        })
      );
    },
    [field, rule_position, reduxDispatch]
  );

  const handleDelete = useCallback(() => {
    if (!field?.field_id) return;
    reduxDispatch(
      actions.fields.removeFieldRule(field.field_id, rule_position)
    );
  }, [field, rule_position, reduxDispatch]);

  const handleAddChild = useCallback(() => {
    if (!field?.field_id) return;
    reduxDispatch(
      actions.fields.addFieldRule(
        field.field_id,
        [...rule_position, "children"],
        {
          rule_id: helpers.fields.generateRuleId(),
          rule_field_id: "",
          rule_value: "",
          children: [],
        }
      )
    );
  }, [field, rule_position, reduxDispatch]);

  return (
    <Wrapper
      className="field-rule"
      ref={handle_ref}
      data-handler-id={handle_id}
      style={{ opacity: is_dragging ? 0.4 : 1 }}
    >
      <div className="field-rule__header flex items-center space-x-3">
        <SelectField
          className="flex-grow"
          options={fieldOptions}
          placeholder="Choose field"
          value={rule.rule_field_id}
          onChange={(event) =>
            handleChange("rule_field_id", event.target.value)
          }
        />
        <SelectField
          className="flex-grow"
          options={fieldOptionOptions}
          placeholder="Choose field option"
          value={rule.rule_value}
          onChange={(event) => handleChange("rule_value", event.target.value)}
        />

        <Button color="danger" onClick={handleDelete} size="small">
          Remove
        </Button>
      </div>

      {rule.children.length > 0 && (
        <DndProvider backend={HTML5Backend}>
          <div className="field-rule__children border border-gray-700 px-4 mb-4">
            {rule.children.map((child, childIndex) => (
              <Dragger
                type={`rule-child-${rule.rule_id}`}
                key={child.rule_id}
                className="rule-dragger"
                id={child.rule_id}
                index={childIndex}
                meta={{
                  rule: child,
                  rule_position: [...rule_position_children, childIndex],
                  index: childIndex,
                  onMove,
                }}
                Component={FieldRule}
                onMove={(...args) => onMove(...args, rule_position_children)}
              />
            ))}
          </div>
        </DndProvider>
      )}

      <div className="text-left">
        <Button color="primary" onClick={handleAddChild} size="small">
          Add new child
        </Button>
      </div>
    </Wrapper>
  );
};

export default React.memo(FieldRule);

const Wrapper = styled.div`
  margin-bottom: 20px;

  .field-rule__children {
    margin-left: 30px;
  }
`;
