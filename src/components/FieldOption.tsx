import React, { useCallback } from "react";
import styled from "styled-components";

import TextField from "./TextField";
import Button from "./Button";
import { OptionDomain } from "../lib/instances/field";
import { useDispatch } from "react-redux";
import actions from "../store/actions";
import { useFieldContext } from "../contexts/FieldContext";

type FieldOptionProps = {
  option: OptionDomain;
  index: number;
  is_dragging: boolean;
  handle_ref: React.RefObject<HTMLDivElement>;
  handle_id: any;
};

/**
 * Name: FieldOption
 */
const FieldOption: React.FC<FieldOptionProps> = ({
  option,
  index,
  is_dragging,
  handle_ref,
  handle_id,
}) => {
  const reduxDispatch = useDispatch();
  const field = useFieldContext();

  const handleChange = useCallback(
    (key: keyof OptionDomain, value: string) => {
      if (!field?.field_id) return;
      option[key] = value;
    },
    [option, field?.field_id]
  );

  const handleDelete = useCallback(() => {
    if (!field?.field_id) return;
    reduxDispatch(
      actions.fields.removeFieldOption(field.field_id, option.option_id)
    );
  }, [field, option.option_id, reduxDispatch]);

  return (
    <Wrapper
      className="field-option flex items-center space-x-3"
      ref={handle_ref}
      style={{ opacity: is_dragging ? 0.4 : 1 }}
      data-handler-id={handle_id}
    >
      <span className="field-option__count">{index + 1}</span>

      <TextField
        className="flex-grow"
        placeholder="Enter option label"
        value={option.option_label}
        onBlur={(event) => handleChange("option_label", event.target.value)}
      />
      <TextField
        className="flex-grow"
        placeholder="Enter option value"
        value={option.option_value}
        onBlur={(event) => handleChange("option_value", event.target.value)}
      />

      <Button color="danger" onClick={handleDelete} size="small">
        Remove
      </Button>
    </Wrapper>
  );
};

export default React.memo(FieldOption);

const Wrapper = styled.div``;
