import React, { useCallback } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import FieldRule from "./FieldRule";
import { FieldDomain } from "../lib/instances/field";
import actions from "../store/actions";
import selector from "../store/selector";
import Button from "./Button";
import helpers from "../helpers";
import Dragger from "./Dragger";

type FieldRulesProps = {
  field_id: FieldDomain["field_id"];
  rule_position?: (number | string)[];
};

/**
 * Name: FieldRules
 */
const FieldRules: React.FC<FieldRulesProps> = ({
  field_id,
  rule_position = [],
}) => {
  const reduxDispatch = useDispatch();
  const rules = useSelector(selector.fields.get(field_id))?.rules;

  const handleNewRule = useCallback(() => {
    reduxDispatch(
      actions.fields.addFieldRule(field_id, rule_position, {
        rule_id: helpers.fields.generateRuleId(),
        rule_value: "",
        rule_field_id: "",
        children: [],
      })
    );
  }, [field_id, rule_position, reduxDispatch]);

  const handleMove = useCallback(
    (
      from_index: number,
      to_index: number,
      position: (number | string)[] = []
    ) => {
      reduxDispatch(
        actions.fields.moveFieldRule(field_id, position, from_index, to_index)
      );
    },
    [field_id, reduxDispatch]
  );

  return (
    <Wrapper>
      <h5 className="text-base font-bold mb-1">Rules</h5>
      <DndProvider backend={HTML5Backend}>
        <div className="field-options__list mb-2">
          {rules?.map((rule, index) => (
            <Dragger
              type="rule"
              key={rule.rule_id}
              className="rule-dragger"
              id={rule.rule_id}
              index={index}
              meta={{
                rule: rule,
                rule_position: [...rule_position, index],
                index,
                onMove: handleMove,
              }}
              Component={FieldRule}
              onMove={handleMove}
            />
          ))}
          {rules?.length === 0 && <li>You have no rules</li>}
        </div>
      </DndProvider>

      <div className="text-right">
        <Button onClick={handleNewRule} size="small">
          Add new rule
        </Button>
      </div>
    </Wrapper>
  );
};

export default React.memo(FieldRules);

const Wrapper = styled.div``;
