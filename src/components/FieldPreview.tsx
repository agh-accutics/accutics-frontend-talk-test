import React, { useMemo, useEffect } from "react";
import styled from "styled-components";
import { usePreviewContext } from "../contexts/PreviewContext";
import helpers from "../helpers";

import { FieldDomain } from "../lib/instances/field";
import SelectField from "./SelectField";

type FieldPreviewProps = {
  field: FieldDomain;
};

/**
 * Name: FieldPreview
 */
const FieldPreview: React.FC<FieldPreviewProps> = ({ field }) => {
  const { data, get, onChange } = usePreviewContext();

  const show = useMemo(() => {
    return helpers.fields.validateRule(field.rules, data);
  }, [field.rules, data]);

  useEffect(() => {
    if (!show && get(field.field_id)) {
      onChange(field.field_id, null);
    }
    // eslint-disable-next-line
  }, [show, field.field_id]);

  if (!onChange || !get || !show) {
    return null;
  }

  return (
    <Wrapper>
      <SelectField
        label={field.field_name}
        value={get(field.field_id) || ""}
        placeholder={`Select a value`}
        onChange={(event) => onChange(field.field_id, event.target.value)}
        options={helpers.fields.fieldOptionsToOptions(field.options)}
      />
    </Wrapper>
  );
};

export default React.memo(FieldPreview);

const Wrapper = styled.div``;
