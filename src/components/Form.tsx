import React, { useCallback } from "react";
import styled from "styled-components";

type FormProps = {
  children: JSX.Element | JSX.Element[];
  onSubmit: React.FormEventHandler<HTMLFormElement>;
};

/**
 * Name: Form
 */
const Form: React.FC<FormProps> = ({ onSubmit, children }) => {
  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();
      event.stopPropagation();
      onSubmit(event);
    },
    [onSubmit]
  );
  return <Wrapper onSubmit={handleSubmit}>{children}</Wrapper>;
};

export default React.memo(Form);

const Wrapper = styled.form``;
