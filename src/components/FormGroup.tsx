import React from "react";
import styled from "styled-components";
import clsx from "clsx";

import FormLabel, { Props as FormLabelProps } from "./FormLabel";

export interface Props {
  className?: string;
  children: JSX.Element;
  label?: FormLabelProps["label"];
  required?: boolean;
}

const FormGroup: React.FC<Props> = ({
  className,
  label,
  required,
  children,
}: Props) => {
  return (
    <Wrapper className={clsx(className, "form-group my-4")}>
      {label && (
        <FormLabel
          className="form-group__label"
          label={label}
          required={required}
        />
      )}
      {children}
    </Wrapper>
  );
};

export default React.memo(FormGroup);

const Wrapper = styled.div``;
