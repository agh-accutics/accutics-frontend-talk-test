import clsx from "clsx";
import React, { useCallback } from "react";
import styled from "styled-components";
import helpers from "../helpers";

import FormGroup, { Props as FormGroupProps } from "./FormGroup";

export type Option = { label: string; value: string; children?: Option[] };

type Props = Omit<FormGroupProps, "children"> & {
  value?: string;
  placeholder?: string;
  options: Option[];
  disabled?: boolean;
  onChange?: React.ChangeEventHandler<HTMLSelectElement>;
};
/**
 * Name: SelectField
 */
const SelectField: React.FC<Props> = React.forwardRef<HTMLSelectElement, Props>(
  (
    {
      value,
      label,
      placeholder,
      options,
      required,
      disabled,
      className,
      onChange,
    },
    ref
  ) => {
    const renderOptions = useCallback((options: Option[], level = 0): any => {
      return options.map((option) => {
        if (option.children) {
          return [
            <option key={option.value} value={option.value}>
              {helpers.fields.getLevelDashes(level)} {option.label}
            </option>,
            renderOptions(option.children, level + 1),
          ];
        }
        return (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        );
      });
    }, []);

    return (
      <FormGroupStyled
        label={label}
        required={required}
        className={clsx("select-field", className)}
      >
        <select
          ref={ref}
          className="select-field__input text-gray-800"
          disabled={disabled}
          value={value || ""}
          onChange={onChange}
        >
          {placeholder && <option value="">{placeholder}</option>}
          {renderOptions(options)}
        </select>
      </FormGroupStyled>
    );
  }
);

export default React.memo(SelectField);

const FormGroupStyled = styled(FormGroup)`
  .select-field__input {
    display: block;
    width: 100%;
    padding: 5px 10px;
    border-radius: 0;
  }
`;
