import React, { useState, useCallback } from "react";
import clsx from "clsx";
import styled from "styled-components";

import FieldContext from "../contexts/FieldContext";
import { FieldDomain } from "../lib/instances/field";
import FieldOptions from "./FieldOptions";
import FieldRules from "./FieldRules";
import Button from "./Button";
import TextField from "./TextField";

type FieldProps = {
  field_id: FieldDomain["field_id"];
  field_name: FieldDomain["field_name"];
  index: number;
  is_dragging: boolean;
  handle_ref: React.RefObject<HTMLDivElement>;
  handle_id: any;
};

/**
 * Name: Field
 */
const Field: React.FC<FieldProps> = ({
  field_id,
  field_name,
  index,
  is_dragging,
  handle_ref,
  handle_id,
}) => {
  const [open, setOpen] = useState(false);
  const [editing, setEditing] = useState(false);

  const handleNameChange = useCallback(() => {
    // @TODO Update the field name in redux here.
  }, []);

  const opacity = is_dragging ? 0.4 : 1;

  return (
    <FieldContext value={{ field_name, field_id }}>
      <Wrapper
        className="field border border-gray-400 mb-3"
        style={{ opacity }}
      >
        <div
          className={clsx(
            "field__header flex items-center px-5 py-3 cursor-pointer hover:bg-gray-600",
            {
              "open border-b border-gray-400": open,
            }
          )}
          ref={handle_ref}
          data-handler-id={handle_id}
          onClick={editing ? undefined : () => setOpen(!open)}
        >
          <div className="field__header-counter font-bold text-sm">
            {index + 1}
          </div>
          <div className="field__header-name text-base flex-grow mx-3">
            {editing ? (
              <TextField
                autoFocus
                placeholder="Enter field name"
                value={field_name}
                onBlur={handleNameChange}
              />
            ) : (
              field_name
            )}
          </div>

          <Button
            className="ml-auto"
            size="small"
            onClick={() => setEditing(!editing)}
          >
            Edit
          </Button>
        </div>

        {open && (
          <div className="field__body px-5 py-3">
            <FieldOptions field_id={field_id} />
            <FieldRules field_id={field_id} />
          </div>
        )}
      </Wrapper>
    </FieldContext>
  );
};

export default React.memo(Field);

const Wrapper = styled.div``;
