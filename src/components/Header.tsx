import React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";

import Container from "./Container";
import clsx from "clsx";

type HeaderProps = {
  className?: string;
};

/**
 * Name: Header
 */
const Header: React.FC<HeaderProps> = ({ className }) => {
  return (
    <Wrapper className={clsx(className, "bg-gray-700 mb-6")}>
      <Container>
        <nav className="menu">
          <NavLink className="menu__item" to="/" exact>
            Setup
          </NavLink>
          <NavLink className="menu__item" to="/test" exact>
            Test
          </NavLink>
        </nav>
      </Container>
    </Wrapper>
  );
};

export default React.memo(Header);

const Wrapper = styled.header`
  padding: 30px 0;

  .menu__item {
    padding: 10px 20px;
    border-bottom: 5px solid transparent;
    margin-right: 15px;

    &.active {
      border-color: #ddd;
    }
  }
`;
