import React, { useState, useCallback } from "react";

import Form from "./Form";
import TextField from "./TextField";
import Button from "./Button";
import { FieldDomain } from "../lib/instances/field";
// import actions from "../store/actions";
// import helpers from "../helpers";

const CreateFieldForm: React.FC = () => {
  const [inputs, setInputs] = useState<Partial<FieldDomain>>({
    field_name: "",
  });

  const handleChange = useCallback(
    (key: keyof FieldDomain, value: string) => {
      return setInputs((prev) => ({ ...prev, [key]: value }));
    },
    [setInputs]
  );

  const handleSubmit = useCallback(() => {
    // Field name is required.
    if (!inputs.field_name) return;

    // @TODO Send the field data to redux.
    console.log("MISSING SUBMITTION HANDLING");
  }, [inputs]);

  return (
    <div className="create-field-form">
      <h5 className="text-lg font-bold mb-4">Create field</h5>

      <Form onSubmit={handleSubmit}>
        <TextField
          label="Field name"
          placeholder="Enter field name"
          required
          value={inputs.field_name || ""}
          onChange={(event) => handleChange("field_name", event.target.value)}
        />
        <Button type="submit" disabled={!inputs.field_name}>
          Create field
        </Button>
      </Form>
    </div>
  );
};

export default React.memo(CreateFieldForm);
