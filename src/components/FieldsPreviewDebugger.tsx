import React from "react";
import styled from "styled-components";

import { usePreviewContext } from "../contexts/PreviewContext";

type FieldsPreviewDebuggerProps = {};

/**
 * Name: FieldsPreviewDebugger
 */
const FieldsPreviewDebugger: React.FC<FieldsPreviewDebuggerProps> = () => {
  const { data } = usePreviewContext();

  return (
    <Wrapper className="preview-data-debugger">
      <b>Current form data:</b>
      <pre className="bg-gray-900 p-5">{JSON.stringify(data, null, 2)}</pre>
    </Wrapper>
  );
};

export default React.memo(FieldsPreviewDebugger);

const Wrapper = styled.pre``;
