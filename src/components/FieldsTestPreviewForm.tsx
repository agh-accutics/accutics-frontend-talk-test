import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";

import PreviewContextProvider from "../contexts/PreviewContext";
import selector from "../store/selector";
import FieldPreview from "./FieldPreview";
import FieldsPreviewDebugger from "./FieldsPreviewDebugger";

type FieldsTestPreviewFormProps = {};

/**
 * Name: FieldsTestPreviewForm
 */
const FieldsTestPreviewForm: React.FC<FieldsTestPreviewFormProps> = () => {
  const fields = useSelector(selector.fields.all);

  return (
    <Wrapper className="fields-test">
      <PreviewContextProvider>
        <div className="fields-test__fields-list">
          {fields.map((field) => {
            return <FieldPreview key={field.field_id} field={field} />;
          })}
        </div>

        <FieldsPreviewDebugger />
      </PreviewContextProvider>
    </Wrapper>
  );
};

export default React.memo(FieldsTestPreviewForm);

const Wrapper = styled.div`
  &:empty::after {
    content: "No fields available";
    display: block;
    font-style: italic;
    font-size: 18px;
  }
`;
