import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import FieldOption from "./FieldOption";
import Button from "./Button";
import { FieldDomain } from "../lib/instances/field";
import selector from "../store/selector";
import actions from "../store/actions";
import Dragger from "./Dragger";
import helpers from "../helpers";

type FieldOptionsProps = {
  field_id: FieldDomain["field_id"];
};

/**
 * Name: FieldOptions
 */
const FieldOptions: React.FC<FieldOptionsProps> = ({ field_id }) => {
  const reduxDispatch = useDispatch();
  const options = useSelector(selector.fields.get(field_id))?.options;
  const handleNewOption = useCallback(() => {
    reduxDispatch(
      actions.fields.addFieldOption(field_id, {
        option_id: helpers.fields.generateOptionId(),
        option_label: "",
        option_value: "",
      })
    );
  }, [field_id, reduxDispatch]);

  const handleMove = useCallback(
    (from_index: number, to_index: number) => {
      reduxDispatch(
        actions.fields.moveFieldOption(field_id, from_index, to_index)
      );
    },
    [field_id, reduxDispatch]
  );

  return (
    <Wrapper className="field-options mb-8">
      <h5 className="text-base font-bold mb-1">Options</h5>
      <DndProvider backend={HTML5Backend}>
        <div className="field-options__list mb-2">
          {options?.map((option, index) => (
            <Dragger
              type="option"
              key={option.option_value}
              className="option-dragger"
              id={option.option_value}
              index={index}
              meta={{
                option,
                index,
              }}
              Component={FieldOption}
              onMove={handleMove}
            />
          ))}
          {options?.length === 0 && <li>You have no options</li>}
        </div>
      </DndProvider>

      <Button onClick={handleNewOption} size="small">
        Add new option
      </Button>
    </Wrapper>
  );
};

export default React.memo(FieldOptions);

const Wrapper = styled.div``;
