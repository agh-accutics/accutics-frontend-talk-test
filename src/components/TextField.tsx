import clsx from "clsx";
import React from "react";
import styled from "styled-components";

import FormGroup, { Props as FormGroupProps } from "./FormGroup";

type Props = Omit<FormGroupProps, "children"> & {
  value?: string;
  type?: "text" | "number";
  placeholder?: string;
  autoFocus?: boolean;
  disabled?: boolean;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  onBlur?: React.FocusEventHandler<HTMLInputElement>;
};
/**
 * Name: TextField
 */
const TextField: React.FC<Props> = React.forwardRef<HTMLInputElement, Props>(
  (
    {
      value,
      type,
      label,
      placeholder,
      required,
      autoFocus,
      disabled,
      className,
      onChange,
      onBlur,
    },
    ref
  ) => {
    return (
      <FormGroupStyled
        label={label}
        required={required}
        className={clsx("text-field", className)}
      >
        <input
          ref={ref}
          autoFocus={autoFocus}
          type={type}
          placeholder={placeholder}
          className="text-field__input text-gray-800"
          disabled={disabled}
          value={onChange ? value || "" : undefined}
          defaultValue={onBlur ? value : undefined}
          onChange={onChange}
          onBlur={onBlur}
        />
      </FormGroupStyled>
    );
  }
);

export default React.memo(TextField);

const FormGroupStyled = styled(FormGroup)`
  .text-field__input {
    display: block;
    width: 100%;
    padding: 5px 10px;
    border-radius: 0;

    &:focus,
    &:active {
      outline: none;
    }
  }
`;
