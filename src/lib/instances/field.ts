import { FieldDto, OptionDto, RuleDto } from "../../api/field";

export type OptionDomain = OptionDto & {
  option_id: string;
};

export type RuleDomain = Omit<RuleDto, "children" | "rule_field_key"> & {
  rule_id: string;
  rule_field_id?: string;
  children: RuleDomain[];
};

export type FieldDomain = Omit<FieldDto, "field_key" | "options" | "rules"> & {
  field_id: string;
  options: OptionDomain[];
  rules: RuleDomain[];
};
