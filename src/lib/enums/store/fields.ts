export enum FieldTypes {
  SET_FIELDS = "fields/set",
  ADD_FIELD = "fields/add-field",
  UPDATE_FIELD = "fields/update-field",
  MOVE_FIELD = "fields/move-field",
  REMOVE_FIELD = "fields/remove-field",

  ADD_FIELD_OPTION = "fields/add-field-option",
  UPDATE_FIELD_OPTION = "fields/update-field-option",
  MOVE_FIELD_OPTION = "fields/move-field-option",
  REMOVE_FIELD_OPTION = "fields/remove-field-option",

  ADD_FIELD_RULE = "fields/add-field-rule",
  UPDATE_FIELD_RULE = "fields/update-field-rule",
  MOVE_FIELD_RULE = "fields/move-field-rule",
  REMOVE_FIELD_RULE = "fields/remove-field-rule",
}
