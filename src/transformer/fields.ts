import { FieldDto, OptionDto, RuleDto } from "../api/field";
import {
  FieldDomain,
  OptionDomain,
  RuleDomain,
} from "./../lib/instances/field";
import helpers from "../helpers";

type TempField = FieldDto & { field_id: string };

export const ruleToDomain = (
  rule: RuleDto,
  fields: TempField[]
): RuleDomain => {
  return {
    rule_id: helpers.fields.generateRuleId(),
    rule_value: rule.rule_value,
    rule_field_id: fields.find(
      (field) => field.field_key === rule.rule_field_key
    )?.field_id,
    children: rule.children.map((rule) => ruleToDomain(rule, fields)),
  };
};

export const optionToDomain = (option: OptionDto): OptionDomain => {
  return {
    ...option,
    option_id: helpers.fields.generateOptionId(),
  };
};

export const toDomain = (
  field: TempField,
  fields: TempField[]
): FieldDomain => {
  return {
    field_id: field.field_id,
    field_name: field.field_name,
    options: field.options.map(optionToDomain),
    rules: field.rules.map((rule) => ruleToDomain(rule, fields)),
  };
};

export const toDomains = (fields: FieldDto[]): FieldDomain[] => {
  const tempFields = fields.map((field) => ({
    ...field,
    field_id: helpers.fields.generateFieldId(),
  }));

  return tempFields.map((field) => toDomain(field, tempFields));
};
