import * as fields from "./fields";

const transformer = {
  fields,
};

export default transformer;
