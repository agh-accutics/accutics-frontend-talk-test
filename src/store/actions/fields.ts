import { RuleDomain } from "./../../lib/instances/field";
import { FieldTypes } from "../../lib/enums/store/fields";
import { FieldDomain, OptionDomain } from "../../lib/instances/field";

export const setFields = (fields: FieldDomain[]) => ({
  type: FieldTypes.SET_FIELDS,
  payload: { fields },
});

// FIELD ACTIONS
export const addField = (field: FieldDomain) => ({
  type: FieldTypes.ADD_FIELD,
  payload: { field },
});

export const updateField = (
  field_id: string,
  field_data: Partial<FieldDomain>
) => ({
  type: FieldTypes.UPDATE_FIELD,
  payload: { field_id, field_data },
});

export const moveField = (from_index: number, to_index: number) => ({
  type: FieldTypes.MOVE_FIELD,
  payload: { from_index, to_index },
});

export const removeField = (field_id: string) => ({
  type: FieldTypes.REMOVE_FIELD,
  payload: { field_id },
});

// FIELD OPTION ACTIONS
export const addFieldOption = (
  field_id: string,
  option_data: Partial<OptionDomain>
) => ({
  type: FieldTypes.ADD_FIELD_OPTION,
  payload: { field_id, option_data },
});
export const updateFieldOption = (
  field_id: string,
  option_id: string,
  option_data: Partial<OptionDomain>
) => ({
  type: FieldTypes.UPDATE_FIELD_OPTION,
  payload: { field_id, option_id, option_data },
});
export const moveFieldOption = (
  field_id: string,
  from_index: number,
  to_index: number
) => ({
  type: FieldTypes.MOVE_FIELD_OPTION,
  payload: { field_id, from_index, to_index },
});
export const removeFieldOption = (field_id: string, option_id: string) => ({
  type: FieldTypes.REMOVE_FIELD_OPTION,
  payload: { field_id, option_id },
});

// FIELD OPTION ACTIONS
export const addFieldRule = (
  field_id: string,
  rule_position: (number | string)[],
  rule_data: Partial<RuleDomain>
) => ({
  type: FieldTypes.ADD_FIELD_RULE,
  payload: { field_id, rule_position, rule_data },
});
export const updateFieldRule = (
  field_id: string,
  rule_position: (number | string)[],
  rule_data: Partial<RuleDomain>
) => ({
  type: FieldTypes.UPDATE_FIELD_RULE,
  payload: { field_id, rule_position, rule_data },
});
export const moveFieldRule = (
  field_id: string,
  rule_position: (number | string)[],
  from_index: number,
  to_index: number
) => ({
  type: FieldTypes.MOVE_FIELD_RULE,
  payload: { field_id, rule_position, from_index, to_index },
});
export const removeFieldRule = (
  field_id: string,
  rule_position: (number | string)[]
) => ({
  type: FieldTypes.REMOVE_FIELD_RULE,
  payload: { field_id, rule_position },
});
