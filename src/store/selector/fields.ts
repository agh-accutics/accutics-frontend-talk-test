import { FieldDomain } from "../../lib/instances/field";
import { RootState } from "../reducers";

export const all = (state: RootState): FieldDomain[] => state.fields.items;
export const get =
  (field_id: FieldDomain["field_id"] | undefined) =>
  (state: RootState): FieldDomain | undefined =>
    state.fields.items.find((field) => field.field_id === field_id);
