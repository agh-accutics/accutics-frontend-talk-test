import * as fields from "./fields";

const selector = {
  fields,
};

export default selector;
