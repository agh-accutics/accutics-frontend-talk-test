import { combineReducers } from "redux";

import fields, { FieldState } from "./fields";

export type RootState = {
  fields: FieldState;
};

export default combineReducers({
  fields,
});
