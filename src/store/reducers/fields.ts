import * as R from "ramda";
import helpers from "../../helpers";

import { FieldTypes } from "../../lib/enums/store/fields";
import { FieldDomain } from "../../lib/instances/field";
import { Action } from "../../lib/instances/store";

export type FieldState = {
  items: FieldDomain[];
};

const defaultState: FieldState = {
  items: [],
};

const fields = (
  state = defaultState,
  { type, payload }: Action
): FieldState => {
  switch (type) {
    case FieldTypes.SET_FIELDS: {
      return { ...state, items: payload.fields };
    }

    case FieldTypes.ADD_FIELD: {
      return { ...state, items: [...state.items, payload.field] };
    }
    case FieldTypes.UPDATE_FIELD: {
      return {
        ...state,
        items: state.items.map((field) =>
          field.field_id === payload.field_id
            ? { ...field, ...payload.field_data }
            : field
        ),
      };
    }
    case FieldTypes.MOVE_FIELD: {
      return {
        ...state,
        items: helpers.array.move(
          state.items,
          payload.to_index,
          payload.from_index
        ),
      };
    }
    case FieldTypes.REMOVE_FIELD: {
      return {
        ...state,
        items: state.items.filter(
          (field) => field.field_id !== payload.field_id
        ),
      };
    }

    case FieldTypes.ADD_FIELD_OPTION: {
      return {
        ...state,
        items: state.items.map((field) =>
          field.field_id === payload.field_id
            ? { ...field, options: [...field.options, payload.option_data] }
            : field
        ),
      };
    }
    case FieldTypes.UPDATE_FIELD_OPTION: {
      return {
        ...state,
        items: state.items.map((field) =>
          field.field_id === payload.field_id
            ? {
                ...field,
                options: field.options.map((option) =>
                  option.option_id === payload.option_id
                    ? { ...option, ...payload.option_data }
                    : option
                ),
              }
            : field
        ),
      };
    }
    case FieldTypes.MOVE_FIELD_OPTION: {
      return {
        ...state,
        items: state.items.map((field) =>
          field.field_id === payload.field_id
            ? {
                ...field,
                options: helpers.array.move(
                  field.options,
                  payload.to_index,
                  payload.from_index
                ),
              }
            : field
        ),
      };
    }
    case FieldTypes.REMOVE_FIELD_OPTION: {
      return {
        ...state,
        items: state.items.map((field) =>
          field.field_id === payload.field_id
            ? {
                ...field,
                options: field.options.filter(
                  (option) => option.option_id !== payload.option_id
                ),
              }
            : field
        ),
      };
    }

    case FieldTypes.ADD_FIELD_RULE: {
      const rule_position: (number | string)[] = payload.rule_position;
      const lens = R.lensPath([...rule_position]);
      const items = state.items.map((field) => {
        if (field.field_id !== payload.field_id) return field;
        const old_data = R.view(lens, field.rules);
        if (rule_position.length === 0) {
          return {
            ...field,
            rules: [...field.rules, payload.rule_data],
          };
        }
        if (!old_data) return field;

        return {
          ...field,
          rules: R.set(lens, [...old_data, payload.rule_data], field.rules),
        };
      });

      return {
        ...state,
        items,
      };
    }
    case FieldTypes.UPDATE_FIELD_RULE: {
      const rule_position: (number | string)[] = payload.rule_position;
      const lens = R.lensPath([...rule_position]);
      const items = state.items.map((field) => {
        if (field.field_id !== payload.field_id) return field;
        const old_data = R.view(lens, field.rules);

        if (!old_data) return field;

        return {
          ...field,
          rules: R.set(
            lens,
            { ...old_data, ...payload.rule_data },
            field.rules
          ),
        };
      });

      return {
        ...state,
        items,
      };
    }
    case FieldTypes.MOVE_FIELD_RULE: {
      const rule_position: (number | string)[] = payload.rule_position;
      const lens = R.lensPath([...rule_position]);
      const items = state.items.map((field) => {
        if (field.field_id !== payload.field_id) return field;
        const old_data = R.view(lens, field.rules);

        if (!old_data) return field;

        return {
          ...field,
          rules: R.set(
            lens,
            helpers.array.move(old_data, payload.to_index, payload.from_index),
            field.rules
          ),
        };
      });
      return {
        ...state,
        items,
      };
    }
    case FieldTypes.REMOVE_FIELD_RULE: {
      const rule_position: (number | string)[] = [...payload.rule_position];
      const rule_index = rule_position.pop();
      const lens = R.lensPath([...rule_position]);
      const items = state.items.map((field) => {
        if (field.field_id !== payload.field_id) return field;
        const old_data = R.view(lens, field.rules);

        if (!old_data) return field;

        return {
          ...field,
          rules: R.set(
            lens,
            old_data.filter((_: any, i: number) => i !== rule_index),
            field.rules
          ),
        };
      });

      return {
        ...state,
        items,
      };
    }
    default:
      break;
  }
  return state;
};

export default fields;
