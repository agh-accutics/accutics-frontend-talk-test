import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import logger from "redux-logger";

import rootReducer, { RootState } from "./reducers";

export default function configureStore(
  preloadedState: Partial<RootState> = {}
) {
  const middlewareEnhancer = applyMiddleware(logger);

  const enhancers = [middlewareEnhancer];
  const composedEnhancers = composeWithDevTools(...enhancers);

  const store = createStore(rootReducer, preloadedState, composedEnhancers);

  return store;
}
