import React from "react";
import { Switch, Route } from "react-router-dom";

import routes from "./routes";

/**
 * Name: Router
 */
const Router = () => {
  return (
    <Switch>
      {routes.map((route) => (
        <Route
          key={route.path}
          exact={route.exact !== false}
          path={route.path}
          component={route.component}
        />
      ))}
    </Switch>
  );
};

export default React.memo(Router);
