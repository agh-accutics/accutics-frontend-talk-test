import React from "react";

import Home from "../views/Home";
import Test from "../views/Test";

type IRoute = {
  path: string;
  component: React.FunctionComponent;
  exact?: boolean;
};

const routes: IRoute[] = [
  { path: "/", component: Home },
  { path: "/test", component: Test },
];

export default routes;
