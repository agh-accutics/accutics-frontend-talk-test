import FIELDS_DATA from "../data/correct-data.json";

export type OptionDto = {
  option_label: string;
  option_value: string;
};

export type RuleDto = {
  rule_field_key: string;
  rule_value: string;
  children: RuleDto[];
};

export type FieldDto = {
  field_key: string;
  field_name: string;
  options: OptionDto[];
  rules: RuleDto[];
};

// Load all the fields.
export const load = (): Promise<FieldDto[]> =>
  new Promise<FieldDto[]>((resolve) => {
    resolve(FIELDS_DATA);
  });
