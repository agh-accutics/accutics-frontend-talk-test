module.exports = {
  purge: {
    content: [
      "./src/**/*.js",
      "./src/**/*.ts",
      "./src/**/*.tsx",
      "./src/**/*.css",
      "./src/**/*.scss",
    ],
  },
  darkMode: "media",
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
