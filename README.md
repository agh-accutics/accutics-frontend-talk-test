# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Get started

In the project directory, you can run:

### `yarn && yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\

## Concepts

This project will give you a setup page where you can edit fields with its options and rules.
Then you will get a _Test_ page where you can test if the rules is setup correctly.

Here is a list of the data types in the project:

**Field** -> _FieldDomain_ type:

```
type FieldDomain = {
    field_id: string;
    field_name: string;
    options: OptionDomain[];
    rules: RuleDomain[];
};
```

**Option** -> _OptionDomain_ type:

```
type OptionDomain = {
    option_id: string;
    option_label: string;
    option_value: string;
};
```

**Rule** -> _RuleDomain_ type:

```
type RuleDomain = {
    rule_id: string;
    rule_field_id?: string;
    rule_value: string;
    children: RuleDomain[];
};
```

## Most interesting files

- `src/components/Fields.tsx` -> Has the list of fields for the Setup
- `src/components/FieldsTestPreviewForm.tsx` -> Has the list of fields for the Test/Preview page
- `src/components/CreateFieldForm.tsx` -> Has missing submittion for create field.
- `src/helpers/fields.tsx` -> Has missing validation helper for field rules.
- `src/components/FieldOption.tsx` -> Has a bug for field updates?

## Know bugs in the system

Here is a list of known bugs or todo's:

- [ ] The create field does not work?
- [ ] The field name is not changeable?
- [ ] The field rules does not work in the test area yet.
- [ ] The field rules should work with the nesting logic.
- [ ] When updating the field option label or value something wierd happen (no redux action?).
